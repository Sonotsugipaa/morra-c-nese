package it.uniupo.uni.morra

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.RadioButton

import kotlinx.android.synthetic.main.activity_main.*
import kotlin.random.Random

class MainActivity : AppCompatActivity() {

    private val drawFun: View.OnClickListener = View.OnClickListener { draw() }
    private val resetFun: View.OnClickListener = View.OnClickListener { reset() }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        reset()
    }


    private fun draw() {
        val rnd: Int = Random.nextInt(0, 3)
        val sel: Int = choiceToInt()
        val cmp: Int = ((rnd - sel) + 3) % 3
        lblVictory.text = when(cmp) {
            0 -> getString(R.string.draw)
            1 -> getString(R.string.lost)
            2 -> getString(R.string.won)
            else -> "?"
        }
        lblChoice.text = when(rnd) {
            0 -> getString(R.string.cpuChoice, getString(R.string.rock))
            1 -> getString(R.string.cpuChoice, getString(R.string.paper))
            2 -> getString(R.string.cpuChoice, getString(R.string.scissors))
            else -> "?"
        }

        btnSubmit.setOnClickListener(resetFun)
        btnSubmit.text = getString(R.string.reset)
        lblVictory.visibility = View.VISIBLE
        lblChoice.visibility = View.VISIBLE
    }

    private fun reset() {
        btnSubmit.setOnClickListener(drawFun)
        btnSubmit.text = getString(R.string.submit)
        lblVictory.visibility = View.INVISIBLE
        lblChoice.visibility = View.INVISIBLE
    }

    private fun choiceToInt(): Int {
        val v: View? = findViewById(selectRps.checkedRadioButtonId)
        if(v is RadioButton) {
            return when(v.text) {
                getString(R.string.radioRock)     -> 0
                getString(R.string.radioPaper)    -> 1
                getString(R.string.radioScissors) -> 2
                else -> -1
            }
        }
        return -1
    }
}
